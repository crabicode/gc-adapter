const GCAdapter = require('../lib/manager');

async function main() {
  const manager = new GCAdapter();

  await manager.sendir({
    host: '192.168.1.195',
    port: 4998,
    output: 1,
    command: {
      frequency: 38000,
      irCode: '172,172,22,64,22,64,22,64,22,21,22,21,22,21,22,21,22,21,22,64,22,64,22,64,22,21,22,21,22,21,22,21,22,21,22,21,22,64,22,21,22,21,22,64,22,21,22,21,22,21,22,64,22,21,22,64,22,64,22,21,22,64,22,64,22,64,22,1820',
      preamble: '',
      repeat: 1,
    },
  });

  console.warn('[INFO] done');
  await manager.destroy();
}

process.on('unhandledRejection', (caughtErr) => {
  throw caughtErr;
});

if (process.env.NODE_ENV !== 'test') {
  main.call(null);
}
