const joi = require('joi');
const Connection = require('./connection');

const SendingParams = joi.object({
  host: joi.string().disallow(''),
  port: joi.number().positive().max(65535),
  output: joi.number().positive().max(65535),
  command: joi.object(),
});

/**
 * @typedef {Object} Command
 * @property {number} repeat
 * @property {number} frequency
 * @property {(string|number[])=} preamble
 * @property {string|number[]} params
 */

/**
 * @typedef {Object} SendingOptions
 * @property {string} host
 * @property {number} port
 * @property {number} output
 * @property {Command} command
 */

class GlobalCacheManager {
  constructor() {
    this.heartbeats = new Map();
    this.connections = new Map();
    this.tid = setInterval(
      this._handleHeartbeat.bind(this),
      5000,
    );
  }

  async _handleHeartbeat() {
    const now = Date.now();
    for (const [id, lastTouched] of this.heartbeats) {
      const con = this.connections.get(id);
      if (now - lastTouched > 60000 && !con.pending) {
        this.heartbeats.delete(id);
        this.connections.delete(id);
        process.nextTick(async () => {
          try { await con.close(); } catch (caughtErr) {
            // supress
          }
        });
      }
    }
  }

  /**
   *
   * @param {...SendingOptions} options
   * @returns {Promise<void>}
   */
  async sendir(options) {
    if (this._destroyed) {
      throw new Error('Failed to use the destroyed manager');
    }

    const { error } = SendingParams.validate(
      options,
      GlobalCacheManager.validation,
    );

    if (error) {
      throw error;
    }

    const {
      host, port, output, command,
    } = options;

    const id = `${host}:${port}`;
    let con = this.connections.get(id);

    if (!con) {
      con = new Connection();
      const cleanup = () => {
        this.heartbeats.delete(id);
        this.connections.delete(id);
      };
      con.once('close', cleanup);
      this.connections.set(id, con);
      this.heartbeats.set(id, Date.now());
      try {
        await con.open(host, port);
      } catch (caughtErr) {
        cleanup();
        throw caughtErr;
      }
    }

    const { preamble, irCode, ...params } = command;

    return con.sendir({
      ...params,
      module: 1,
      port: output,
      preamble: preamble && typeof preamble === 'string' ?
        preamble.split(',').map(Number) : preamble || [],
      params: typeof irCode === 'string' ?
        irCode.split(',').map(Number) : irCode,
    });
  }

  /**
   * Closes all connections and stops the heartbeat
   */
  async destroy() {
    if (this._destroyed) {
      return;
    }
    this._destroyed = true;
    clearInterval(this.tid);
    const connections = Array.from(this.connections.values());
    const promise = Promise.all(connections.map(con => con.close()));
    this.connections.clear();
    await promise;
  }
}

GlobalCacheManager.validation = {
  convert: false,
  abortEarly: true,
  presence: 'required',
  allowUnknown: false,
};


module.exports = GlobalCacheManager;
