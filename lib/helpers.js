const makeDeferred = () => {
  let resolve = null;
  let reject = null;
  const promise = new Promise((_resolve, _reject) => {
    resolve = _resolve;
    reject = _reject;
  });
  return {
    resolve,
    reject,
    promise,
  };
};

function makeReader(callback) {
  let payload = '';
  return function reader(bytes) {
    const str = bytes.toString();
    for (const char of str) {
      if (char === '\r') {
        callback(payload);
        payload = '';
      } else {
        payload += char;
      }
    }
  };
}

module.exports = {
  makeReader,
  makeDeferred,
};
