const { Socket } = require('net');
const { EventEmitter } = require('events');
const { makeDeferred, makeReader } = require('./helpers');
const Joi = require('./joi');

const SendingParams = Joi.object({
  module: Joi.number().positive(),
  port: Joi.number().positive(),
  frequency: Joi.number().positive().min(15000).max(500000),
  repeat: Joi.number().positive().min(1).max(20),
  preamble: Joi.array().pairs().items(Joi.number().positive().min(4).max(50000))
    .optional(),
  params: Joi.array().pairs().min(1).items(Joi.number().positive().min(4).max(50000)),
});

class GlobalCacheConnection extends EventEmitter {
  constructor() {
    super();
    this._ack = 0;
    this._queue = [];
    this._socket = null;
    this._handleData = this._handleData.bind(this);
    this._handleClose = this._handleClose.bind(this);
  }

  open(host, port) {
    if (this._socket) {
      return null;
    }
    return new Promise((resolve, reject) => {
      const socket = new Socket();
      const reader = makeReader(this._handleData);
      socket.on('data', reader);
      socket.once('connect', resolve);
      socket.once('error', reject);
      socket.once('close', this._handleClose);
      socket.unref();
      socket.connect({ host, port });
      this._socket = socket;
    });
  }

  _handleClose() {
    // reject pending requests
    this._queue.forEach(deferred =>
      deferred.reject(new Error('Connection closed')));
    // reset ack & flush queue
    this._ack = 0;
    this._queue = [];
    this._socket = null;
    this.emit('close');
  }

  _handleData(data) {
    const deferred = this._queue.shift();
    if (!deferred) {
      return;
    }
    try {
      const match = data.match(/[^,]+/g);
      if (!match) {
        throw new Error('Unprocessed data');
      }
      const [command, ...params] = match;
      switch (command) {
        case 'busyIR': {
          throw new Error('busyIR');
        }
        case 'completeir': {
          deferred.resolve();
          break;
        }
        default:
          if (/^ERR/.test(command)) {
            const [errorCode] = params;
            throw new Error(`ERR ${errorCode}`);
          }
          throw new Error(`Unhandled ${data}`);
      }
    } catch (caughtErr) {
      deferred.reject(caughtErr);
    }
  }

  static _createSendir(request) {
    const {
      module,
      port,
      id,
      frequency,
      repeat,
      preamble,
      params,
    } = request;

    let payload = '';
    payload += 'sendir';
    payload += `,${module}:${port}`;
    payload += `,${id}`;
    payload += `,${frequency}`;
    payload += `,${repeat}`;
    offset: {
      if (preamble && preamble.length > 0) {
        payload += `,${(preamble.length + 1)}`; // offset
        payload += `,${preamble.join(',')}`; // preamble
        break offset;
      }
      payload += ',1'; // offset
      break offset;
    }
    payload += `,${params.join(',')}`; // payload
    payload += '\r'; // end
    return payload;
  }

  async sendir(params) {
    // ensure connection is open
    if (!this.connected) {
      throw new Error('Connection is not open');
    }

    // validate params
    const { error } = SendingParams.validate(
      params,
      GlobalCacheConnection.validation,
    );

    if (error) {
      throw error;
    }

    // create payload
    const id = (this._ack++) & 0xffff;
    const payload = GlobalCacheConnection._createSendir({
      ...params,
      id,
    });

    // queue the request
    const deferred = makeDeferred();
    this._queue.push(deferred);

    // wait for current pending item in queue
    try {
      const [pending] = this._queue;
      if (pending && pending !== deferred) {
        await pending.promise;
      }
    } catch (caughtErr) {
      if (!this.connected) {
        throw new Error('Connection is not open');
      }
    }

    // send payload
    try { await this._send(payload); } catch (caughtErr) {
      const index = this._queue.findIndex(deferred);
      this._queue.splice(index, 1);
      deferred.reject(caughtErr);
    }

    return deferred.promise;
  }

  _send(bytes) {
    if (!this._socket) {
      throw new Error('Connection is not open');
    }
    return new Promise((resolve) => {
      this._socket.write(bytes, resolve);
    });
  }

  close() {
    if (!this._socket) {
      return null;
    }
    return new Promise((resolve, reject) => {
      this._socket.once('close', (hadError) => {
        if (hadError) {
          reject(new Error('Socket closed with error'));
          return;
        }
        resolve();
      });
      this._socket.destroy();
    });
  }

  get connected() {
    return this._socket !== null && !this._socket.destroyed;
  }

  get pending() {
    return this.connected && this._queue.length > 0;
  }
}

GlobalCacheConnection.validation = {
  convert: false,
  abortEarly: true,
  presence: 'required',
  allowUnknown: false,
};

module.exports = GlobalCacheConnection;
