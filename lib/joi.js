const Joi = require('joi');

module.exports = Joi.extend(joi => ({
  base: joi.array(),
  name: 'array',
  language: {
    pairs: 'must contain pairs',
  },
  rules: [
    {
      name: 'pairs',
      validate(params, value, state, options) {
        if (value.length % 2 >= 1) {
          return this.createError('array.pairs', { v: value }, state, options);
        }
        return value;
      },
    },
  ],
}));
